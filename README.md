# Snowflake Proxy for Home Assistant

The Tor Project's Snowflake can now be used on devices running Home Assistant!

## About

This add-on allows you to help people in censored countries access the Internet
without restrictions using [WebRTC](https://en.wikipedia.org/wiki/WebRTC), the
technology that powers the world's most popular real-time communication
software.

**This add-on has not been endorsed by The Tor Project.**

- You can find a detailed explanation on how Snowflake works on
  [The Tor Project's website](https://snowflake.torproject.org).
- [📚 Read the full add-on documentation][addon-documentation]

## License

This repository is distributed under the [MIT License][license].

Snowflake, which is not being distributed in this repository, is licensed under
the terms of the
[BSD 3-clause License](https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/blob/main/LICENSE).

[addon-documentation]: snowflake/DOCS.md
[license]: LICENSE
